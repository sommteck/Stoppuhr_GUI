using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stopuhr
{
    public partial class Form1 : Form
    {
        DateTime dt;

        public Form1()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /*  Tick
            Der Timer ruft eine Anzahl Ticks ab, die Datum und Uhrzeit dieser Instanz darstellen.
            Ein Tick stellt 100 Nanosekunden bzw. eine zehnmillionste Sekunde dar.
            In einer Millisekunde sind demnal 10.000 Ticks enthalten
         */
        
        private void timer1_Tick(object sender, EventArgs e)
        // Diese Methode wurde anhand des Steuerelemnts "Timer" instanziert
        {
            TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            /*
             * Objekt an Hand der Klasse "TimeSpan" instanziert.
             * Als Parameter wird die Berechnung übergeben
             * Die Ausgabe erfolgt im folgendem Format:
             * "Minuten:Sekunden:Zehntel- und Hundertstelsekunden"
            */
            lbl_output.Text = ts.ToString("mm\\:ss\\.ff");
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            lbl_output.Text = null;
        }

        private void cmd_start_stop_Click(object sender, EventArgs e)
        {
            if (cmd_start_stop.Text == "&Start")
            {
                // Aktuelle Zeit ermitteln
                dt = DateTime.Now;
                // Timer starten
                timer1.Start();
                // Beschriftung des Start-/Stop-Buttons ändern
                cmd_start_stop.Text = "&Stop";
            }
            else 
            {
                // Timer stoppen
                timer1.Stop();
                // Beschriftung des Start-/Stop-Buttons ändern
                cmd_start_stop.Text = "&Start";
            }
        }
    }
}
